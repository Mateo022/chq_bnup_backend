-- ------------------------------------------------------------------
-- 						Todos los derechos Reservados
-- 							Softipal S.A.S 2023
-- ------------------------------------------------------------------
-- Nombre: sp_addUser
-- Descripción: Agregará un nuevo registro de usuario
-- Fecha: 04/04/2023
-- Autor: MateoC
-- Versión: 1.0
-- OD: BNUP-45
-- ------------------------------------------------------------------
-- 							Histórico de cambios
-- ------------------------------------------------------------------
-- 		Fecha		OD			Autor 			Descripción 
-- ------------------------------------------------------------------
--    04/04/2023      BNUP-45     MateoC     Se crea procedimiento

DELIMITER $$

CREATE PROCEDURE `sp_addUser`(in _userId varchar(100),in _nameFull varchar(150),in _email varchar(150))
    COMMENT '{"version":"2","OD":"BNUP-45","date":"2023-04-04"}'
BEGIN
	INSERT IGNORE INTO `tus_user` (`userId`,`nameFull`,`email`) VALUES (_userId,_nameFull,_email);

END $$

DELIMITER ;