<?php

namespace App\Models\DataAccess;

class DaUser {
    function __construct(){}
    
    /**
     * Agregará un nuevo registro de usuario
     * @param String $userId Identificador externo del usuario
     * @param String $nameFull Nombre completo del usuario
     * @param String $email Correo electronico del usuario
     * @return type
     */
    public function addUSer($userId,$nameFull,$email)
    {
        return app('db')->select("call sp_addUser(?,?,?)", array($userId,$nameFull,$email));
    }
}