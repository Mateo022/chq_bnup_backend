<?php

namespace App\Models\ModuleAccess;

use App\Models\DataAccess\DaUser;

class MaUser
{
    private $status;
    private $message;
    private $DaUser;

    function __construct()
    {
        $this->status = false;
        $this->message = "Error de conexión";
        $this->DaUser = new DaUser();
    }

    public function addUser($userId, $nameFull, $email)
    {
        $this->DaUser->addUser($userId, $nameFull, $email);

        return response()->json([
                    'success' => $this->status,
                    'message' => $this->message,
                    'addUser' => true
        ]);
    }
}