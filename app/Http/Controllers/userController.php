<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;
use App\Models\ModuleAccess\MaUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class userController extends Controller
{
    public $MaUser; 
    
    function __construct()
    {
        $this->MaUser = new MaUser();
    }

    // Crear usuario
    public function addUser(Request $request)
    {
        $newUserData = $request->input();
        $this->MaUser->addUser($newUserData['userId'], $newUserData['fullName'], $newUserData['email']);
    }

    public function getListUser()
    {
        $users = DB::table('tus_user')->get();
        return response()->json($users);
    }
}
