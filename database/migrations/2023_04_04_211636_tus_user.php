<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TusUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tus_user', function (Blueprint $table)
        {
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->string('userId',100);
            $table->string('nameFull',150);
            $table->string('email',150);
            $table->json('additionalData')->nullable();
            $table->primary("userId");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tus_user');
    }
}
