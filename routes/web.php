<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return view('src.index')->with('id', "12345")->with('name', "Mateo Ceballos")->with('email', "Mateo@softipal.com");;
});

$router->get('getList', 'userController@getListUser');
$router->post('addUser', 'userController@addUser');
