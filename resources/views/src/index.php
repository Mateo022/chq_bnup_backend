<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Bienes nacionales de uso publico</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <!-- <link rel="stylesheet" href="styles.da5bd9e7fcb4076be213.css"> -->
</head>

<body>

	<input type="hidden" id="pal_userId" value="<?php echo $id ?>">
	<input type="hidden" id="pal_lisPersonNameFull" value="<?php echo $name ?>">
	<input type="hidden" id="pal_lisPersonContactEmailPrimary" value="<?php echo $email ?>">

	<div id="main_div">
		<app-root> </app-root>
	</div>

	<!-- <script src="runtime.7b63b9fd40098a2e8207.js" defer></script>
	<script src="polyfills.94daefd414b8355106ab.js" defer></script>
	<script src="scripts.7a11ae8a1a16ac07b0f1.js" defer></script>
	<script src="main.90be5fd1f95bda61ab5e.js" defer></script>
	<script src="js/resize.js"></script> -->
	
</body>

</html>